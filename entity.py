import psycopg2
import psycopg2.extras

__all__ = ['Entity']


class DatabaseError(Exception):
    pass


class NotFoundError(Exception):
    pass


class ModifiedError(Exception):
    pass


class Entity(object):
    db = None

    # ORM part 1

    __delete_query = 'DELETE FROM "{table}" WHERE {table}_id=%s'
    __insert_query = 'INSERT INTO "{table}" ({columns}) \
                      VALUES ({placeholders}) RETURNING "{table}_id"'
    __list_query = 'SELECT * FROM "{table}"'
    __select_query = 'SELECT * FROM "{table}" WHERE {table}_id=%s'
    __update_query = 'UPDATE "{table}" SET {columns} WHERE {table}_id=%s'

    # ORM part 2
    __parent_query = 'SELECT * FROM "{table}" WHERE {parent}_id=%s'
    __sibling_query = 'SELECT * FROM "{sibling}" NATURAL JOIN "{join_table}" \
                       WHERE {table}_id=%s'
    __update_children = 'UPDATE "{table}" SET {parent}_id=%s \
                         WHERE {table}_id IN ({children})'

    def __init__(self, id=None):
        if self.__class__.db is None:
            raise DatabaseError

        self.__cursor = self.__class__.db.cursor(
            cursor_factory=psycopg2.extras.DictCursor
        )
        self.__fields = {}
        self.__id = id
        self.__loaded = False
        self.__modified = False
        self.__table = self.__class__.__name__.lower()

    def __getattr__(self, name):
        # check, if instance is modified and throw an exception
        # get corresponding data from database if needed
        # check, if requested property name is in current class
        #    columns, parents, children or siblings and call corresponding
        #    getter with name as an argument
        # throw an exception, if attribute is unrecognized
        if self.__modified:
            raise ModifiedError

        self.__load()

        if name in self._columns:
            return self._get_column(name)
        elif name in self._parents:
            return self._get_parent(name)
        elif name in self._children.keys():
            return self._get_children(name)
        elif name in self._siblings.keys():
            return self._get_siblings(name)
        else:
            raise AttributeError

    def __setattr__(self, name, value):
        # check, if requested property name is in current class
        #    columns, parents, children or siblings and call corresponding
        # setter with name and value as arguments or use default
        # implementation
        if name in self._columns:
            self._set_column(name, value)
        elif name in self._parents:
            self._set_parent(name, value)
        else:
            # super()
            super(Entity, self).__setattr__(name, value)

    def __execute_query(self, query, args=None):
        # execute an sql statement and handle exceptions together with
        #    transactions
        try:
            if args is None:
                self.__cursor.execute(query)
            else:
                self.__cursor.execute(query, args)
        except:
            self.__class__.db.rollback()
            raise DatabaseError

    def __insert(self):
        # generate an insert query string from fields keys and values and
        #    execute it
        # use prepared statements
        # save an insert id
        columns = ', '.join(['{0}'.format(column)
                            for column in self.__fields.keys()])
        placeholders = ', '.join(['\'{0}\''.format(value)
                                 for value in self.__fields.values()])

        self.__execute_query(self.__insert_query.format(
            table=self.__table, columns=columns, placeholders=placeholders))
        self.__id = self.__cursor.fetchone()[0]

    def __load(self):
        # if current instance is not loaded yet - execute select statement and
        # store it's result as an associative array (fields), where column
        # names used as keys
        if self.__loaded:
            return

        self.__execute_query(
            self.__select_query.format(table=self.__table), (self.__id,))
        self.__fields = dict(self.__cursor.fetchone())
        self.__loaded = True

    def __update(self):
        # generate an update query string from fields keys and values and
        #    execute it
        # use prepared statements
        columns = ', '.join(['{0} = \'{1}\''.format(column, values)
                            for column, values in self.__fields.items()])
        self.__execute_query(
            self.__update_query.format(table=self.__table, columns=columns),
            (self.__id,)
        )

    def _get_children(self, name):
        # return an array of child entity instances
        # each child instance must have an id and be filled with data
        import models

        child_title = self._children[name]
        self.__cursor.execute(self.__parent_query.format(
            table=child_title.lower(), parent=self.__table), (self.__id,))
        child_cls = getattr(models, child_title)
        rows = self.__cursor.fetchall()

        return child_cls._rows_to_entities(rows)

    def _get_column(self, name):
        # return value from fields array by <table>_<name> as a key
        return self.__fields['{}_{}'.format(self.__table, name)]

    def _get_parent(self, name):
        # ORM part 2
        # get parent id from fields with <name>_id as a key
        # return an instance of parent entity class with an appropriate id
        import models

        id = self.__fields['{}_id'.format(name)]
        parent_cls = getattr(models, name.title())

        return parent_cls(id)

    def _get_siblings(self, name):
        # ORM part 2
        # get parent id from fields with <name>_id as a key
        # return an array of sibling entity instances
        # each sibling instance must have an id and be filled with data
        import models

        sibling_title = self._siblings[name]
        sibling = sibling_title.lower()
        join_table = '__'.join(sorted([sibling, self.__table]))
        self.__cursor.execute(self.__sibling_query.format(
            sibling=sibling, join_table=join_table, table=self.__table), (self.__id,))
        sibling_cls = getattr(models, sibling_title)
        rows = self.__cursor.fetchall()

        return sibling_cls._rows_to_entities(rows)

    def _set_column(self, name, value):
        # put new value into fields array with <table>_<name> as a key
        self.__fields['{}_{}'.format(self.__table, name)] = value

        self.__modified = True

    def _set_parent(self, name, value):
        # ORM part 2
        # put new value into fields array with <name>_id as a key
        # value can be a number or an instance of Entity subclass
        buf_value = value
        if isinstance(value, Entity):
            buf_value = value.id
        self.__fields['{}_id'.format(name)] = buf_value

        self.__modified = True

    @classmethod
    def set_database(cls, database, user, password):
        __class__.db = psycopg2.connect(
            database=database, user=user, password=password)
        __class__.db.autocommit = True

    @classmethod
    def all(cls):
        # get ALL rows with ALL columns from corresponding table
        # for each row create an instance of appropriate class
        # each instance must be filled with column data, a correct id and
        #    MUST NOT query a database for own fields any more
        # return an array of instances
        cursor = cls.db.cursor(
            cursor_factory=psycopg2.extras.DictCursor
        )
        cursor.execute(cls.__list_query.format(table=cls.__name__.lower()))

        return cls._rows_to_entities(cursor.fetchall())

    @classmethod
    def _rows_to_entities(cls, rows):
        table_name = cls.__name__.lower()
        for item in rows:
            instance = cls()
            instance.__id = item['{}_{}'.format(table_name, 'id')]
            instance.__fields = dict(item)
            instance.loaded = True
            yield instance

    def delete(self):
        # execute delete query with appropriate id
        if self.__id:
            self.__execute_query(
                self.__delete_query.format(table=self.__table), (self.__id,))
            self.__loaded = False
        else:
            raise NotFoundError

    @property
    def id(self):
        return self.__id

    @property
    def created(self):
        # try to guess yourself
        return self._get_column('created')

    @property
    def updated(self):
        # try to guess yourself
        return self._get_column('updated')

    def save(self):
        # execute either insert or update query, depending on instance id
        if not self.id:
            self.__insert()
        else:
            self.__update()
        self.__modified = False
